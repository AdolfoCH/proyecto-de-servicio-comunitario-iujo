<?php

use Illuminate\Database\Seeder;

class ParishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Santa Rosalía',
            'ext_parish' => '6,1 Km²',
            'population' => '190.282 Hab.',
        ]);

        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => '23 de Enero',
            'ext_parish' => '2,5 Km²',
            'population' => '138.990 Hab.',
        ]);

        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Altagracia',
            'ext_parish' => '3,7 Km²',
            'population' => '67.700 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Antímano',
            'ext_parish' => '29,5 Km²',
            'population' => '214.437 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Candelaria',
            'ext_parish' => '1,2 Km²',
            'population' => '101.088 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Caricuao',
            'ext_parish' => '24,8 Km²',
            'population' => '248.200 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Catedral',
            'ext_parish' => '1,0 Km²',
            'population' => '10.540 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Coche',
            'ext_parish' => '13 Km²',
            'population' => '79.830 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'El Junquito',
            'ext_parish' => '56 Km²',
            'population' => '63.800 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'El Paraíso',
            'ext_parish' => '10,4 Km²',
            'population' => '188.830 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'El Recreo',
            'ext_parish' => '18,1 Km²',
            'population' => '144.480 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'El Valle',
            'ext_parish' => '31,1 Km²',
            'population' => '231.117 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'La Pastora',
            'ext_parish' => '7,6 Km²',
            'population' => '129.980 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'La Vega',
            'ext_parish' => '12,9 Km²',
            'population' => '202.206 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Macarao',
            'ext_parish' => '131,4 Km²',
            'population' => '70.217 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'San Agustín',
            'ext_parish' => '1,7 Km²',
            'population' => '83.200 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'San Bernandino',
            'ext_parish' => '9,9 Km²',
            'population' => '49.621	Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'San José',
            'ext_parish' => '2,6 Km²',
            'population' => '59.980 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'San Juan',
            'ext_parish' => '3,8 Km²',
            'population' => '177.947 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'San Pedro',
            'ext_parish' => '6,7 Km²',
            'population' => '82.000 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Santa Teresa',
            'ext_parish' => '0,8 Km²',
            'population' => '45.000 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 1,
            'parish_name' => 'Sucre',
            'ext_parish' => '58,3 Km²',
            'population' => '451.079 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 2,
            'parish_name' => 'Nuestra Señora del Rosario',
            'ext_parish' => '73 km²',
            'population' => '211.841 Hab',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 2,
            'parish_name' => 'El Cafetal',
            'ext_parish' => '9 km²',
            'population' => '55.130 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 2,
            'parish_name' => 'Las Minas',
            'ext_parish' => '4 km²',
            'population' => '51.441 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 3,
            'parish_name' => 'Chacao',
            'ext_parish' => '13 km²',
            'population' => '70.713 Hab',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 4,
            'parish_name' => 'El Hatillo',
            'ext_parish' => '81 km²',
            'population' => '75.910 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 5,
            'parish_name' => 'Leoncio Martínez',
            'ext_parish' => '23 km²',
            'population' => '63.118 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 5,    
            'parish_name' => 'Petare',
            'ext_parish' => '40 km²',
            'population' => '799.237 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 5,
            'parish_name' => 'Caucaguita',
            'ext_parish' => '54 km²',
            'population' => '64.048 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 5,
            'parish_name' => 'Filas de Mariches',
            'ext_parish' => '36 km²',
            'population' => '34.274 Hab.',
        ]);
        
        DB::table('parishes')->insert([
            'municipalitie_id' => 5,
            'parish_name' => 'La Dolorita',
            'ext_parish' => '11 km²',
            'population' => '84.041 Hab.',
        ]);
    }
}
