<?php

use Illuminate\Database\Seeder;

class MunicipalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('municipalities')->insert([
            'mun_name' => 'Libertador',
            'ext_mun' => '433 km²',
            'population' => '2.925.132 Hab.',
        ]);

        DB::table('municipalities')->insert([
            'mun_name' => 'Baruta',
            'ext_mun' => '	86 km²',
            'population' => '330.626 Hab.',
        ]);

        DB::table('municipalities')->insert([
            'mun_name' => 'Chacao',
            'ext_mun' => '13 km²',
            'population' => '70.713 Hab',
        ]);

        DB::table('municipalities')->insert([
            'mun_name' => 'El Hatillo',
            'ext_mun' => '81 km²',
            'population' => '75.910 Hab.',
        ]);

        DB::table('municipalities')->insert([
            'mun_name' => 'Sucre',
            'ext_mun' => '164 km²',
            'population' => '675.680 Hab.',
        ]);
    }
}
