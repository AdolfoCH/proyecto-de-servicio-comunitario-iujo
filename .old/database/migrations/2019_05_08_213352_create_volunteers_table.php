<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photoVoluntary')->nullable();
            $table->string('namev');
            $table->string('last_namev');
            $table->integer('dni');
            $table->date('birth_date');
            $table->bigInteger('principal_numb');
            $table->string('email')->unique();
            $table->unsignedInteger('group_id');
            $table->bigInteger('secondary_numb')->nullable();
            $table->longText('address')->nullable();
            $table->string('some_discapacity')->nullable();
            $table->boolean('available');
            $table->timestamps();

            $table->foreign('group_id')->references('id')
            ->on('groups')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
