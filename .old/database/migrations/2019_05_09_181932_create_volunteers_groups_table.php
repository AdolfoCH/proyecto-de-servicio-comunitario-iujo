<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('volunteer_id');
            $table->unsignedInteger('group_id');
            $table->timestamps();

            $table->foreign('volunteer_id')->references('id')->on('volunteers')
            ->onUpdate('cascade');
            $table->foreign('group_id')->references('id')->on('groups')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers_groups');
    }
}
