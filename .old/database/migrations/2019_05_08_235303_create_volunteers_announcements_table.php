<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers_announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('announcement_id');
            $table->unsignedInteger('volunteer_id');
            $table->boolean('attended');
            $table->longText('observation');
            $table->timestamps();

            $table->foreign('announcement_id')->references('id')->on('announcements')->onUpdate('cascade');
            $table->foreign('volunteer_id')->references('id')->on('volunteers')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers_announcements');
    }
}
