<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Municipalitie;
use App\Volunteer;
use DB;
class VolunteersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipalities = Municipalitie::all();
        $volunteers =DB::table('volunteers')
                    ->join('groups','volunteers.group_id','=','groups.id')
                    ->join('parishes','groups.parish_id','=','parishes.id')
                    ->where('volunteers.available', TRUE)
                    ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                    'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                    'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                    'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                    'parishes.id as parish_id','parishes.parish_name')
                    ->get();
        // dd($volunteers);
        return view('layouts.volunteers.index', compact('municipalities','volunteers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipalities = Municipalitie::all();
        
        return view('layouts.volunteers.create', compact('municipalities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request);
        // var_dump($request->photoVoluntary);
        $image = $request->file('photoVoluntary');
        $imageName = $image->getClientOriginalName();
        $upload_success = $image->move(public_path('img/volunteers/'),$imageName);
        // dd($upload_success);

        if ($upload_success) {
            $volunteer = new Volunteer();
            $volunteer->namev = $request->namev;
            $volunteer->last_namev = $request->last_namev;
            $volunteer->dni = $request->dni;
            $volunteer->birth_date = $request->birth_date;
            $volunteer->principal_numb = $request->principal_numb;
            $volunteer->email = $request->email;
            $volunteer->group_id = $request->group_id;
            $volunteer->secondary_numb = $request->secondary_numb;
            $volunteer->address = $request->address;
            $volunteer->some_discapacity = $request->some_discapacity;
            $volunteer->available = TRUE;
            $volunteer->photoVoluntary = $imageName;
            $volunteer->save();
        }
        
        $municipalities = Municipalitie::all();
        $volunteers =DB::table('volunteers')
                    ->join('groups','volunteers.group_id','=','groups.id')
                    ->join('parishes','groups.parish_id','=','parishes.id')
                    ->where('volunteers.available', TRUE)
                    ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                    'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                    'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                    'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                    'parishes.id as parish_id','parishes.parish_name')
                    ->get();
        // dd($volunteers);
        return view('layouts.volunteers.index', compact('municipalities','volunteers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $municipalities = Municipalitie::all();
        $volunteer = DB::table('volunteers')
                    ->where('volunteers.id',$id)
                    ->join('groups','volunteers.group_id','=','groups.id')
                    ->join('parishes','groups.parish_id','=','parishes.id')
                    ->where('volunteers.available', TRUE)
                    ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                    'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                    'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                    'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                    'parishes.id as parish_id','parishes.parish_name')
                    ->get();
        // dd($volunteer);

        return view('layouts.volunteers.show',compact('volunteer','municipalities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $municipalities = Municipalitie::all();
        $volunteer = DB::table('volunteers')
                    ->where('volunteers.id',$id)
                    ->join('groups','volunteers.group_id','=','groups.id')
                    ->join('parishes','groups.parish_id','=','parishes.id')
                    ->where('volunteers.available', TRUE)
                    ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                    'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                    'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                    'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                    'parishes.id as parish_id','parishes.parish_name')
                    ->get();
        // dd($volunteer);

        return view('layouts.volunteers.edit', compact('municipalities','volunteer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // dd($request);
        if($request->municipalitie_id == null || $request->parish_id == null || $request->group_id == null ){
            $dataUpdate = [
                "namev" => $request->namev,
                "last_namev" => $request->last_namev,
                "dni" => $request->dni,
                "birth_date" => $request->birth_date,
                "principal_numb" => $request->principal_numb,
                "secondary_numb" => $request->secondary_numb,
                "email" => $request->email,
                "address" => $request->address,
                "some_discapacity" => $request->some_discapacity
            ];

            $group = Volunteer::where('id',$id)->update($dataUpdate);
            $municipalities = Municipalitie::all();
            $volunteers =DB::table('volunteers')
                        ->join('groups','volunteers.group_id','=','groups.id')
                        ->join('parishes','groups.parish_id','=','parishes.id')
                        ->where('volunteers.available', TRUE)
                        ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                        'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                        'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                        'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                        'parishes.id as parish_id','parishes.parish_name')
                        ->get();
            // dd($volunteers);
            return view('layouts.volunteers.index', compact('municipalities','volunteers'));

        } else {
            $dataUpdate = [
                "namev" => $request->namev,
                "last_namev" => $request->last_namev,
                "dni" => $request->dni,
                "birth_date" => $request->birth_date,
                "principal_numb" => $request->principal_numb,
                "secondary_numb" => $request->secondary_numb,
                "email" => $request->email,
                "address" => $request->address,
                "group_id" => $request->group_id,
                "some_discapacity" => $request->some_discapacity
            ];

            $group = Volunteer::where('id',$id)->update($dataUpdate);
            $municipalities = Municipalitie::all();
            $volunteers =DB::table('volunteers')
                        ->join('groups','volunteers.group_id','=','groups.id')
                        ->join('parishes','groups.parish_id','=','parishes.id')
                        ->where('volunteers.available', TRUE)
                        ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                        'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                        'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                        'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                        'parishes.id as parish_id','parishes.parish_name')
                        ->get();
            // dd($volunteers);
            return view('layouts.volunteers.index', compact('municipalities','volunteers'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $volunteer = Volunteer::where('id',$id)->update(['available' => false]);

        $municipalities = Municipalitie::all();
        $volunteers =DB::table('volunteers')
                        ->join('groups','volunteers.group_id','=','groups.id')
                        ->join('parishes','groups.parish_id','=','parishes.id')
                        ->where('volunteers.available', TRUE)
                        ->select('volunteers.id', 'volunteers.photoVoluntary','volunteers.namev',
                        'volunteers.last_namev','volunteers.dni','volunteers.birth_date','volunteers.principal_numb',
                        'volunteers.email','volunteers.secondary_numb','volunteers.address','volunteers.some_discapacity',
                        'volunteers.available as available_vo','groups.name_group','groups.num_contact','groups.available as available_gr',
                        'parishes.id as parish_id','parishes.parish_name')
                        ->get();

        return view('layouts.volunteers.index', compact('municipalities','volunteers'));
    }

    public function getVolunteersBy($id){
        return Volunteer::where('group_id',$id)->get();
    }
}
