<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parish;
class ParishesController extends Controller
{
    public function getParishBy($id){
        return Parish::where('municipalitie_id','=',$id)->get();
    }
}
