<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Municipalitie;
use App\Group;
use DB;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = DB::table('groups')->where('available',true)
                  ->join('parishes','groups.parish_id','=','parishes.id')
                  ->join('municipalities','parishes.municipalitie_id','=','municipalities.id')
                  ->select('groups.id as group_id','groups.name_group','groups.num_contact','parishes.parish_name','municipalities.mun_name')
                  ->get();
        // dd($groups);

        return response()->json(['groups' => $groups, 'code' => 200]);

        // return view('layouts.groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipalities = Municipalitie::all();
        dd($municipalities);

        return view('layouts.groups.create',compact(
            'municipalities'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $group = new Group();
        $group->name_group = $request->name_group;
        $group->num_contact = $request->num_contact;
        $group->available = TRUE;
        $group->parish_id = $request->parish_id;
        $group->save();
    
        return response()->json(['group_created' => $group, 'code' => 200, 'message' => 'Group created succesfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = DB::table('groups')->where('groups.id',$id)
                     ->join('parishes','groups.parish_id','=','parishes.id')
                     ->join('municipalities','parishes.municipalitie_id','=','municipalities.id')
                     ->select('groups.id as group_id','groups.name_group','groups.num_contact','parishes.parish_name','municipalities.mun_name')
                  ->get();

        return view('layouts.groups.show',compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $group = DB::table('groups')->where('groups.id',$id)
                     ->join('parishes','groups.parish_id','=','parishes.id')
                     ->join('municipalities','parishes.municipalitie_id','=','municipalities.id')
                     ->select('groups.id as group_id','groups.name_group','groups.num_contact','parishes.parish_name','municipalities.mun_name')
                  ->get();

        // dd($group[0]->group_id);
        $municipalities = Municipalitie::all();
        
        // dd($group->id);
        return view('layouts.groups.edit',compact('group','municipalities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);

        $dataUpdate = [
            "name_group" => $request->name_group,
            "num_contact" => $request->num_contact,
            "parish_id" => $request->parish_id,
        ];
        // dd($dataUpdate);

        $group = Group::where('id',$id)->update($dataUpdate);
        // $group->update($dataUpdate);

        $groups = DB::table('groups')->where('available',true)
                  ->join('parishes','groups.parish_id','=','parishes.id')
                  ->join('municipalities','parishes.municipalitie_id','=','municipalities.id')
                  ->select('groups.id as group_id','groups.name_group','groups.num_contact','parishes.parish_name','municipalities.mun_name')
                  ->get();

        return view('layouts.groups.index', compact('groups'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::where('id',$id)->update(['available' => false]);

        $groups = DB::table('groups')->where('available',true)
                  ->join('parishes','groups.parish_id','=','parishes.id')
                  ->join('municipalities','parishes.municipalitie_id','=','municipalities.id')
                  ->select('groups.id as group_id','groups.name_group','groups.num_contact','parishes.parish_name','municipalities.mun_name')
                  ->get();

        return view('layouts.groups.index', compact('groups'));
    }

    public function getGroupsBy($id){
        return Group::where('parish_id',$id)->get();
    }
}
