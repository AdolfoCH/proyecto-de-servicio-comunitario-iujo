<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $table = 'volunteers';

    protected $fillable = [
        'photoVoluntary','namev','last_namev','dni','birth_date','principal_numb','email',
        'group_id','secondary_numb','address','some_discapacity',
        'available','created_at','updated_at'
    ];

    public function group(){
        return $this->hasOne('App\Group','group_id');
    }

    public function announcement(){
        return $this->belongsToMany('App\Announcement');
    }
}
