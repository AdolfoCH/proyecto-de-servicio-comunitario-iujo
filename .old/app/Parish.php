<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parish extends Model
{
    protected $table = 'parishes';

    protected $fillable = [
        'municipalitie_id','parish_name','ext_parish','population','created_at', 'updated_at'
    ];

    public function municipalitie(){
        return $this->hasOne('App\Municipalitie','id');
    }

    public function group(){
        return $this->hasMany('App\Group');
    }
}
