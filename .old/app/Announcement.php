<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = 'announcements';

    protected $fillable = [
        'title', 'type_eventuality_id','cause','assistance',
    ];

    public function typeEventuality(){
        return $this->hasOne('App\TypeEventuality','type_eventuality_id');
    }

    public function volunteer(){
        return $this->belongsToMany('App\Volunteer');
    }
}
