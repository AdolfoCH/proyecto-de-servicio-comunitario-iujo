<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeEventuality extends Model
{
    protected $table = 'type_eventualities';

    protected $fillable = [
        'type_event_name'
    ];

    public function announcement(){
        return $this->belongsTo('Announcement','type_eventuality_id');
    }
}
