<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'name_group','num_contact','available','parish_id'
    ];

    public function parish(){
        return $this->hasOne('App\Parish','id');
    }

    public function volunteers(){
        return $this->hasMany('App\Volunteer');
    }
}
