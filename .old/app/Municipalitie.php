<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipalitie extends Model
{
    protected $table = 'municipalities';

    protected $fillable = [
        'mun_name','ext_mun','population','created_at','updated_at'
    ];

    public function parishes(){
        return $this->belongsTo('App\Parish','municipalitie_id');
    }
}
