
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
  
Vue.use(VueRouter)
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('letterhead-component',require('./components/LetterHeadComponent.vue').default);
Vue.component('welcome-component',require('./components/WelcomeComponent.vue').default);
Vue.component('login-component',require('./components/LogInComponent').default);

const routes = [
    { path: '/', component: require('./components/WelcomeComponent.vue'), name: 'Welcome' },
    { path: '/login', component: require('./components/LogInComponent.vue'), name: 'LogIn'}
]

const router = new VueRouter({
    routes 
  })

const app = new Vue({
    router
}).$mount('#app')
