@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h5>Convocatorias realizadas</h5>
                    </div>
                    <div class="m-2 text-right">
                        <a class="btn btn-success" href="{{ route('announcements.create') }}">Nuevo</a>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Eventualidad</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Motivo</th>
                                <th scope="col">Asistencia</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
