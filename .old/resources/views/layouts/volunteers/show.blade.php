@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 m-auto">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                    <h5>Voluntario: {{ $volunteer[0]->namev }} {{ $volunteer[0]->last_namev }}</h5>
                    </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="namev">Nombre del voluntario</label>
                            <input class="form-control" type="text" name="namev" value="{{ $volunteer[0]->namev }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="last_namev">Apellido del Voluntario</label>
                            <input class="form-control" type="text" name="last_namev" value="{{ $volunteer[0]->last_namev }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="dni">Cédula del voluntario</label>
                                <input class="form-control" type="number" name="dni" value="{{ $volunteer[0]->dni }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="birth_date">Fecha de nacimiento del voluntario</label>
                                <input class="form-control" type="date" name="birth_date" value="{{ $volunteer[0]->birth_date }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="principal_numb">Número de contacto</label>
                            <input class="form-control" type="number" name="principal_numb" value="{{ $volunteer[0]->principal_numb }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="principal_numb">Número alterno para contacto</label>
                            <input class="form-control" type="number" name="secondary_numb" value="{{ $volunteer[0]->secondary_numb }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <label for="email">Correo eléctronico</label>
                            <input class="form-control" type="email" name="email" value="{{ $volunteer[0]->email }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="parish_id">Parroquía del grupo al cual pertenece</label>
                                    <select class="form-control" id="parishes" name="parish_id">
                                    <option value=" ">{{ $volunteer[0]->parish_name }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="parish_id">Grupo del cual es miembro:</label>
                                    <select class="form-control" id="groups" name="group_id">
                                        <option value=" ">{{ $volunteer[0]->name_group }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="address">Dirección de habitación</label>
                                <textarea class="form-control" name="address" cols="5" rows="5">
                                        {{ $volunteer[0]->address }}
                                </textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="some_discapacity">Sí posee alguna discapacidad, ingrese su descripción
                                        aquí</label>
                                    <textarea class="form-control" name="some_discapacity" cols="5" rows="5" placeholder="{{ $volunteer[0]->some_discapacity }}"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group text-center">
                                        <a class="nav-link" href="{{ url('volunteers') }}">
                                            Volver
                                        </a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection