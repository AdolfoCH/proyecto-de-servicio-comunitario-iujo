@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 m-auto">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h5>Añadir nuevo voluntario</h5>
                    </div>
                    <form action="{{ action('VolunteersController@store') }}" method="POST"
                        enctype='multipart/form-data'>
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="namev">Nombre del voluntario</label>
                                <input class="form-control" type="text" name="namev">
                            </div>
                            <div class="col-sm-6">
                                <label for="last_namev">Apellido del Voluntario</label>
                                <input class="form-control" type="text" name="last_namev">
                            </div>
                            <div class="col-sm-12 mt-2">
                                <div class="form-group">
                                    <label for="photo">Insertar foto del voluntario</label>
                                    <div class="custom-file">
                                        <input class="btn btn-success" type="file" name="photoVoluntary">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="dni">Cédula del voluntario</label>
                                <input class="form-control" type="number" name="dni">
                            </div>
                            <div class="col-sm-6">
                                <label for="birth_date">Fecha de nacimiento del voluntario</label>
                                <input class="form-control" type="date" name="birth_date">
                            </div>
                            <div class="col-sm-6">
                                <label for="principal_numb">Número de contacto</label>
                                <input class="form-control" type="number" name="principal_numb">
                            </div>
                            <div class="col-sm-6">
                                <label for="principal_numb">Número alterno para contacto</label>
                                <input class="form-control" type="number" name="secondary_numb">
                            </div>
                            <div class="col-sm-6">
                                <label for="email">Correo eléctronico</label>
                                <input class="form-control" type="email" name="email">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="municipalitie_id">Municipio del grupo al cual pertenece</label>
                                    <select class="form-control" id="municipalities" name="municipalitie_id">
                                        <option value=" ">Seleccionar municipio ...</option>
                                        @foreach ($municipalities as $municipalitie)
                                        <option value="{{ $municipalitie->id }}">{{ $municipalitie->mun_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="parish_id">Parroquía del grupo al cual pertenece</label>
                                    <select class="form-control" id="parishes" name="parish_id">
                                        <option value=" ">Seleccionar parroquía ...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="parish_id">Grupo del cual es miembro:</label>
                                    <select class="form-control" id="groups" name="group_id">
                                        <option value=" ">Seleccionar Grupo ...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="address">Dirección de habitación</label>
                                    <textarea class="form-control" name="address" cols="5" rows="5">
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="some_discapacity">Sí posee alguna discapacidad, ingrese su descripción
                                        aquí</label>
                                    <textarea class="form-control" name="some_discapacity" cols="5" rows="5">
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group text-center">
                                    <input class="btn btn-primary" type="submit" value="Guardar">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#municipalities").change(function () {
            var url = 'http://localhost:8000/';
            var municipalities = $(this).val();
            $.get(url + 'parishesByMunicipalities/' + municipalities, function (data) {
                console.log(data)
                var parish_select = '<option value="">Seleccionar Parroquía ...</option>';
                for (let i = 0; i < data.length; i++) {
                    parish_select += '<option value="' + data[i].id + '">' + data[i]
                        .parish_name + '</option>';
                }
                $("#parishes").html(parish_select)
            });
        });
    });

</script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#parishes").change(function () {
            var url = 'http://localhost:8000/';
            var parishes = $(this).val();
            $.get(url + 'groupsByParishes/' + parishes, function (data) {
                console.log(data)
                var group_select = '<option value="">Seleccionar grupo ...</option>';
                for (let i = 0; i < data.length; i++) {
                    group_select += '<option value="' + data[i].id + '">' + data[i].name_group +
                        '</option>';
                }
                $("#groups").html(group_select)
            });
        });
    });

</script>

<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection
