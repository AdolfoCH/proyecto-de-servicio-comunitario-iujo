@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h5>Voluntarios de Protección Civil Distrito Capital</h5>
                    </div>
                    <div class="m-2 text-right  ">
                        <a class="btn btn-success" href="{{ route('volunteers.create') }}">Nuevo</a>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">

                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Apellido</th>
                                        <th scope="col">cédula</th>
                                        <th scope="col">Grupo</th>
                                        <th scope="col">Parroquía</th>
                                        <th scope="col">foto</th>
                                        <th scope="col">Acerca de</th>
                                        <th scope="col">Actualizar</th>
                                        <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($volunteers as $volunteer)
                                    <tr>
                                        <td>{{ $volunteer->namev }}</td>
                                        <td>{{ $volunteer->last_namev }}</td>
                                        <td>{{ $volunteer->dni }}</td>
                                        <td>{{ $volunteer->name_group }}</td>
                                        <td>{{ $volunteer->parish_name }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#voluntario{{ $volunteer->id }}">
                                                Ver foto
                                            </button>
                                        </td>
                                        <td>
                                            <form
                                                action="{{ action('VolunteersController@show',['$id' => $volunteer->id]) }}"
                                                method="GET">
                                                @csrf
                                                <input class="btn btn-link m-1" type="submit" value="Ver información">
                                            </form>
                                        </td>
                                        <td>
                                            <form
                                                action="{{ action('VolunteersController@edit',['$id' => $volunteer->id]) }}"
                                                method="GET">
                                                @csrf
                                                <input class="btn btn-primary m-1 text-white" type="submit"
                                                    value="Modificar">
                                            </form>
                                        </td>
                                        <td>
                                            <form
                                                action="{{ action('VolunteersController@destroy',['$id' => $volunteer->id]) }}"
                                                method="POST">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <input class="btn btn-danger m-1 text-white" type="submit"
                                                    value="Borrar">
                                            </form>
                                        </td>
                                        <div class="modal fade" id="voluntario{{ $volunteer->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Foto del
                                                            voluntario: {{ $volunteer->namev }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img class="img-fluid"
                                                            src="img/volunteers/{{ $volunteer->photoVoluntary }}">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
