@extends('layouts.app')

@section('content')
<div class="col-sm-6 m-auto">
        <div class="card">
            <div class="card-body">
                <div class="card-title text-center">
                    <h5>{{ $group[0]->name_group }}</h5>
                </div>
                <form action="{{ action('GroupsController@update',['$id' => $group[0]->group_id]) }}" method="POST">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name_group">Nombre del grupo</label>
                            <input class="form-control" type="text" name="name_group"
                                value="{{ $group[0]->name_group }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="num_contact">Número de contacto del grupo</label>
                            <input class="form-control" type="number" name="num_contact"
                                value="{{ $group[0]->num_contact }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="municipalitie_id">Municipio al cual pertenece actualmente</label>
                            <input type="text" class="form-control" value="{{ $group[0]->mun_name }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="parish_id">Parroquía a la cual pertenece actualmente</label>
                            <input type="text" class="form-control" value="{{ $group[0]->parish_name }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group text-center">
                            <a class="btn btn-link text-center" href="{{ url('groups') }}">Volver</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection