@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-6 m-auto">
        <div class="card">
            <div class="card-body">
                <div class="card-title text-center">
                    <h5>Modificar grupo existente</h5>
                </div>
                <form action="{{ action('GroupsController@update',['$id' => $group[0]->group_id]) }}" method="POST">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name_group">Nombre del grupo</label>
                            <input class="form-control" type="text" name="name_group"
                                value="{{ $group[0]->name_group }}">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="num_contact">Número de contacto del grupo</label>
                            <input class="form-control" type="number" name="num_contact"
                                value="{{ $group[0]->num_contact }}">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="municipalitie_id">Municipio al cual pertenecía</label>
                            <input type="text" class="form-control" value="{{ $group[0]->mun_name }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="municipalitie_id">Municipio al cual cambiará</label>
                            <select class="form-control" id="municipalities" name="municipalitie_id">
                                <option value=" ">Seleccionar municipio ...</option>
                                @foreach ($municipalities as $municipalitie)
                                <option value="{{ $municipalitie->id }}">{{ $municipalitie->mun_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="parish_id">Parroquía a la cual pertenecía</label>
                            <input type="text" class="form-control" value="{{ $group[0]->parish_name }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="parish_id">Parroquía a la cual cambiará</label>
                            <select class="form-control" id="parishes" name="parish_id">
                                <option value=" ">Seleccionar parroquía ...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group text-center">
                            <input class="btn btn-primary" type="submit" value="Guardar">
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#municipalities").change(function () {
            var url = 'http://localhost:8000/';
            var municipalities = $(this).val();
            $.get(url + 'parishesByMunicipalities/' + municipalities, function (data) {
                   console.log(data)
                var parish_select = '<option value="">Seleccionar Parroquía ...</option>';
                for (let i = 0; i < data.length; i++) {
                    parish_select += '<option value="' + data[i].id + '">' + data[i]
                        .parish_name + '</option>';
                }
                $("#parishes").html(parish_select)
            });
        });
    });

</script>
@endsection
