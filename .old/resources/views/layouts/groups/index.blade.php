@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 ">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h5>Grupos de voluntarios existentes</h5>
                    </div>
                    <div class="m-2 text-right">
                        <a class="btn btn-success" href="{{ route('groups.create') }}">Nuevo</a>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre del grupo</th>
                                <th scope="col">Número de contacto</th>
                                <th scope="col">Parroquía</th>
                                <th scope="col">Municipio</th>
                                <th scope="col">Acerca de</th>
                                <th scope="col">Actualizar</th>
                                <th scope="col">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($groups as $group)
                            <tr>
                                <td>{{ $group->group_id }}</td>
                                <td>{{ $group->name_group }}</td>
                                <td>{{ $group->num_contact }}</td>
                                <td>{{ $group->parish_name }}</td>
                                <td>{{ $group->mun_name }}</td>
                                <td>
                                    <form action="{{ action('GroupsController@show',['$id' => $group->group_id]) }}"
                                        method="GET">
                                        @csrf
                                        <input class="btn btn-link m-1" type="submit" value="Ver información">
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ action('GroupsController@edit',['$id' => $group->group_id]) }}"
                                        method="GET">
                                        @csrf
                                        <input class="btn btn-primary m-1 text-white" type="submit" value="Modificar">
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ action('GroupsController@destroy',['$id' => $group->group_id]) }}"
                                        method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <input class="btn btn-danger m-1 text-white" type="submit" value="Borrar">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
