@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 m-auto">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h5>Crear de nuevo grupo</h5>
                    </div>
                    <form action="{{ action('GroupsController@store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name_group">Nombre del grupo</label>
                                    <input class="form-control" type="text" name="name_group"
                                        placeholder="Nombre del grupo nuevo">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="num_contact">Número de contacto del grupo</label>
                                    <input class="form-control" type="number" name="num_contact"
                                        placeholder="Numero de contacto del grupo nuevo">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="municipalitie_id">Municipio al cual pertenece</label>
                                    <select class="form-control" id="municipalities" name="municipalitie_id">
                                        <option value=" ">Seleccionar municipio ...</option>
                                            @foreach ($municipalities as $municipalitie)
                                                <option value="{{ $municipalitie->id }}">{{ $municipalitie->mun_name }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="parish_id">Parroquía a la cual pertenece</label>
                                    <select class="form-control" id="parishes" name="parish_id">
                                        <option value=" ">Seleccionar parroquía ...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group text-center">
                                    <input class="btn btn-primary" type="submit" value="Guardar">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#municipalities").change(function(){
            var url = 'http://localhost:8000/';
            var municipalities = $(this).val();
                $.get(url + 'parishesByMunicipalities/' + municipalities, function(data){
                    console.log(data)
                    var parish_select = '<option value="">Seleccionar Parroquía ...</option>';
                    for (let i = 0; i < data.length; i++) {
                        parish_select += '<option value="'+ data[i].id +'">'+ data[i].parish_name +'</option>';
                    }
                    $("#parishes").html(parish_select)
                });
        });
    });

</script>
@endsection
