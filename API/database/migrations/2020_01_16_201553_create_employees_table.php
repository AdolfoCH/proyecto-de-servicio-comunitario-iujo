<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_person_id')->comment('ID con el que se asocia a un empleado con una persona');
            $table->integer('employee_speciality_id')->comment('ID con el que se referenciará a una persona con una especialidad, esta especialidad va a estar contenida dentro de la tabla elements y será parte de la tabla groups como hijo del conjunto Especialidad.');
            $table->integer('employee_municipalitie_id')->comment('ID con el que se referenciará a una persona con un municipio, este municipio va a estar contenido dentro de la tabla elements y será parte de la tabla groups como hijo del conjunto Parroquía.');
            $table->integer('employee_group_id')->comment('ID con el que se referenciará a una persona con una agrupación, esta agrupación va a estar contenido dentro de la tabla elements y será parte de la tabla groups como hijo del conjunto Agrupación.');
            $table->date('employee_entry_date')->comment('Fecha de entrada del empleado.');
            $table->date('employee_egress_date')->comment('Fecha de salida del empleado.');
            $table->json('employee_activity_periods')->comment('Periodos de actividad como empleado.');
            $table->string('employee_photo')->comment('Foto del empleado.');
            $table->boolean('employee_status')->comment('Status del empleado en cuestion, los cuales pueden ser true o false, que significan estar activo o desactivo respectivamente.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
