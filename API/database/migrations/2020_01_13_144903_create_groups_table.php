<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('group_name',100)->comment('Nombre del grupo que contendrá elementos relacionados con el. Ejm: group_name = Venezuela; contendrá los estados del mismo.');
            $table->text('group_desc',255)->comment('Descripción del grupo');
            $table->boolean('group_status')->default(true)->nullable(true)->comment('Status del grupo en cuestion, los cuales pueden ser true o false, que significan estar activo o desactivo respectivamente.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
