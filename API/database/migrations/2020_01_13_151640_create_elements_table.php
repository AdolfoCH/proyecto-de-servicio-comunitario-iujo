<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('element_group_id')->comment('Hace referencia a un grupo mediante un id.');
            $table->string('element_name',100)->comment('Nombre del elemento que pertenecera a un grupo referenciado con el campo: element_group_id.');
            $table->text('element_desc',255)->comment('Descripción del elemento.');
            $table->boolean('element_status')->default(true)->comment('Status del elemento en cuestion, los cuales pueden ser true o false, que significan estar activo o desactivo respectivamente.');
            $table->timestamps();

            $table->foreign('element_group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
