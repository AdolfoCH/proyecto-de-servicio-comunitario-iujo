<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            'group_name' => 'Países',
            'group_desc' => 'La palabra país puede referirse a una nación o a una región. Es el principal sinónimo de estado nacional.',
            'group_status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('groups')->insert([
            'group_name' => 'Estados',
            'group_desc' => 'Comunidad social con una organización política común y un territorio y órganos de gobierno propios que es soberana e independiente políticamente de otras comunidades.',
            'group_status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table()->insert([
            'group_name' => 'Municipios',
            'group_desc' => 'Un municipio es una entidad administrativa que puede agrupar una sola localidad o varias y que puede hacer referencia a una ciudad o un pueblo.',
            'group_status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table()->insert([
            'group_name' => 'Parroquía',
            'group_desc' => 'es la denominación de algunas entidades subnacionales en diferentes países.',
            'group_status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
