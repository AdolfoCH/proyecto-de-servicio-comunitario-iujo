<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elements')->insert([
            'element_group_id' => 1,
            'element_name' => 'Venezuela',
            'element_desc' => 'Elemento perteneciente al grupo países.',
            'element_status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
