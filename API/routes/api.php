<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login')->name('login');
Route::post('register', 'API\UserController@register')->name('register');

//NOTE Rutas protegidas del API.
Route::group(['middleware' => 'auth:api'], function(){

    Route::post('details', 'API\UserController@details')->name('details');

//SECTION Rutas del controlador Group de la aplicación.
    Route::get('/groups', 'GroupController@index');
    Route::post('/groups','GroupController@store');
    Route::post('groups/filter','GroupController@filterByName');
    Route::get('/groups/{id}','GroupController@show');
    Route::put('/groups/{id}','GroupController@update');

//SECTION Rutas del controlador Element de la aplicación.
    Route::get('/elements', 'ElementController@index');
    Route::post('/elements/filter', 'ElementController@filterByGroup');
    Route::post('/elements','ElementController@store');
    Route::get('/elements/{id}','ElementController@show');
    Route::put('/elements/{id}','ElementController@update');    

//SECTION Rutas del controlador Roles de la aplicación.
    Route::get('/roles', 'RolesController@index');
    Route::post('/roles','RolesController@store');
    Route::post('/roles/givePermission','RolesController@givePermissionToRole');
    Route::put('/roles/{id}','RolesController@update');

//SECTION Rutas del controlador Permisos de la aplicación.
    Route::get('/permission', 'PermissionsController@index');
    Route::post('/permission','PermissionsController@store');
    Route::put('/permission/{id}','PermissionsController@update');
});

