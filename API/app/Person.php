<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'peoples';

    protected $fillable = [
        'person_name','person_last_name','person_ci','person_address','person_date_birth'
    ];
}
