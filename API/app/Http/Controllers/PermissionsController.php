<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Validator;


class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = DB::table('permissions')->get();

        if (count($permissions) == 0) {
            return Response()->json([
                'data' => NULL,
                'message' => 'Havent permissions created',
                'code' => 404
            ]);
        }

        return Response()->json([
            'data' => $permissions,
            'message' => 'get permissions susccesfully',
            'code' => 200
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->name as $name) {

            $validator = Validator::make(
                $request->all(), [
                    'name' => 'required'
                ]
            );

            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }

            $permission = Permission::create(['name' => $name]);
            
            $permissions = array();

            array_push($permissions, $permission);
        }

        return Response()->json([   
            'data' => $request->name,
            'message' => 'Store permissions succesfully',
            'code' => 200
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:2' 
            ]
        );

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $permissionUpdated = Group::where('id',$id)->update([
            'name' => $request->name 
        ]);

        if(count($permissionUpdated) == 0){
            return Response()->json([
                'data' => NULL,
                'message' => 'It cannot be modified because this permission was not found',
                'code' => 404
            ]);
        }

        return Response()->json([
            'data' => $permissionUpdated,
            'message' => 'Updated permissions successfully',
            'code' => 200
        ]);
    }

}
