<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Group as Group;
use Validator;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = DB::table('groups')->where('group_status',true)->get();

        if (count($groups) == 0) {
            return Response()->json([
                'data' => null,
                'message' => 'No data to show',
                'code' => 204   
            ]);
        }

        return Response()->json([
            'data' => $groups,
            'message' => 'Processed request successfully',
            'code' => 200
        ]);
    }

    /**
     *  Filter a rows by a parameters in the request.
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @return \Illuminate\Http\Response
     */
    public function filterByName(Request $request)
    {
        dd($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'group_name' => 'required|min:3',
                'group_desc' => 'required|min:15'
            ]
        );

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $group = new Group();
        $group->group_name = $request->group_name;
        $group->group_desc = $request->group_desc;

        $group->save();

        return Response()->json([
            'data' => $group,
            'message' => 'Successful row added to groups table',
            'code' => 200
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $group = DB::table('groups')->select('*')->where('id', $id)->get();
        
        if (count($group) == 0) {
            return Response()->json([
                'data' => NULL,
                'message' => 'Group not found',
                'code' => 404
            ]);
        }

        return Response()->json([
            'data' => $group,
            'message' => 'Successfully show row',
            'code' => 200
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make(
            $request->all(), [
                'group_name' => 'required|min:3',
                'group_desc' => 'required|min:15',
                'group_status' => 'boolean'
            ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $groupUpdated = Group::where('id',$id)->update([
            'group_name' => $request->group_name,
            'group_desc' => $request->group_desc,
            //NOTE Cuando no se especifica el estatus del grupo por defecto lo coloca en true.
            'group_status' => $request->group_status = (false) ? false : true
        ]);
        
        return Response()->json([
            // 'data' => $groupUpdated,
            'message' => 'Successfully row updated',
            'code' => 200
        ]);
    }
}
