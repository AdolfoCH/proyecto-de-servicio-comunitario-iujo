<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Element as Element;
use Validator;

class ElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elements = DB::table('elements')->where('element_status',true)->get();

        if (count($elements) == 0) {
            return Response()->json([
                'data' => null,
                'message' => 'No data to show',
                'code' => 204   
            ]);
        }

        return Response()->json([
            'data' => $elements,
            'message' => 'Processed request successfully',
            'code' => 200
        ]);
    }

    /**
     * Filter the rows of elements table by a parameters sended in a request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filterByGroup(Request $request)
    {
        $elements = DB::table('elements')->where('element_group_id',$request->element_group_id)->get();
        
        if(count($elements) == 0){
            return Response()->json([
                'data' => NULL,
                'message' => 'No groups found',
                'code' => 400
            ]);
        }

        return Response()->json([
            'data' => $elements,
            'message' => count($elements).' '.'matches were found in your search',
            'code' => 200
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'element_group_id' => 'required',
            'element_name' => 'required|min:2',
            'element_desc' => 'required|min:15'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $element = new Element();
        $element->element_group_id = $request->element_group_id;
        $element->element_name = $request->element_name;
        $element->element_desc = $request->element_desc;
        $element->save();

        return Response()->json([
            'data' => $element,
            'message' => 'Successful row added to element table',
            'code' => 200
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $element = DB::table('elements')->where('id',$id)->get();

        if (count($element) == 0) {
            return Response()->json([
                'data' => NULL,
                'message' => 'Element not found',
                'code' => 404
            ]);
        }
        
        return Response()->json([
            'data' => $element,
            'message' => 'Successfully show row',
            'code' => 200        
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
