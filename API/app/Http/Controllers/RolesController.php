<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;



class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = DB::table('roles')->get();
        
        if (count($roles) == 0) {
            
            return Response()->json([
                'data' => NULL,
                'message' => 'Havent roles created',
                'code' => 404
            ]);

        } else {
            return Response()->json([
                'data' => $roles,
                'message' => 'get roles succesfully',
                'code' => 200
            ]);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $roles = [];
        $guard_name = 'api';

        $validator = Validator::make(
            $request->all(), [
                'roles' => 'required'
            ]
        );

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        foreach ($request->roles as $role) {
            
            array_push($roles, Role::findOrCreate($role, $guard_name));
        }

        return Response()->json([   
            'data' => $roles,
            'message' => 'Store roles succesfully',
            'code' => 200
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required'
            ]
        );

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $roleUpdated = Role::findById($id)->update([ 'name' => $request->name]);

        if(!$roleUpdated){
            return Response()->json([
                'data' => NULL,
                'message' => 'It cannot be modified because this role was not found',
                'code' => 404
            ]);
        }

        return Response()->json([
            'data' => $request->name,
            'message' => 'Updated roles successfully',
            'code' => 200
        ]);
    }
    
    /**
     * 
     */
    protected function givePermissionToRole(Request $request){  
        
            $validator = Validator::make(
                $request->all(),[
                    'role' => 'required'
                ]
            );

            $findedRole = Role::findByName($request->role, 'api');
            // dd($findedRole);
            if(empty($findedRole)){
                return Response()->json([
                    'data' => $findedRole,
                    'message' => 'Role not found',
                    'code' => 404
                ]);
            }

            foreach ($request->permissions as $permission) {
                
                $permissionFinded = Permission::findOrCreate($permission, 'api');
                $findedRole->givePermissionTo($permission);
            }

            return Response()->json([
                'data' => [$findedRole, $request->permissions],
                'message' => 'Permissions have been successfully added to the specified role',
                'code' => 200
            ]);
    }
}
