<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $table = 'elements';

    protected $fillable = [
        'element_group_id','element_name', 'element_desc','element_status'
    ];

    
}
