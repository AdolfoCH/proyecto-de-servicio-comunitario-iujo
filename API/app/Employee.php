<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'employee_person_id','employee_speciality_id','employee_municipalitie_id','employee_group_id','employee_entry_date', 'employee_egress_date','employee_activity_periods','employee_photo','employee_status'
    ];
}
